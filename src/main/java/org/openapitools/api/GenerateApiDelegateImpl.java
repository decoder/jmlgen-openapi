package org.openapitools.api;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.decoder.jmlgen.PkmJmlGen;

import java.util.Properties;

import javax.annotation.PostConstruct;


@Service
public class GenerateApiDelegateImpl implements GenerateApiDelegate {

    @PostConstruct
    private void init() {
    	
    }

    @Override
    public ResponseEntity<String> generatePost(String project, String invocationId, String key) {
		
    	if(key == null || key.trim().length() < 1) {
    		return ResponseEntity.status(401).build();
    	}
    	Properties config = ApiUtil.getPkmConfiguration();
    	config.setProperty("apikey", key);
    	PkmJmlGen jmlGenerator = new PkmJmlGen(config);
    	String okMsg = "Successful JML generation";
		try {
			jmlGenerator.injectJmlIntoPKM(project, invocationId, false, false); // Do NOT log out!!! (and not verbose)
		} catch (Exception e) {
			// Filter exceptions related to flat tree (no java package)
			if(e.getMessage().contains("Only directories are allowed as root path")) {
				okMsg = "Successful without JML generation (no java package, or flat tree)";
			} else { // Error
				e.printStackTrace();
				return ResponseEntity.status(405).body("{\"jmlgen\":\"" + ApiUtil.exceptionToSingleLine(e) + "\"}");
			}
		}

        return ResponseEntity.ok().body("{\"jmlgen\":\"" + okMsg + "\"}");
    }

}
