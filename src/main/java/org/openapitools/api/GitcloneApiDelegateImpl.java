package org.openapitools.api;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.decoder.pkm.client.PkmClient;
import eu.decoder.pkm.client.PkmProjectLoader;
import eu.decoder.pkm.utils.GitCloner;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Properties;

import javax.annotation.PostConstruct;


@Service
public class GitcloneApiDelegateImpl implements GitcloneApiDelegate {

    @PostConstruct
    private void init() {
    	
    }
    
    @Override
    public ResponseEntity<String> gitclonePost(String database, String repo, String extension, String key) {
		
    	if(key == null || key.trim().length() < 1) {
    		return ResponseEntity.status(401).build();
    	}

    	try {
    		// Clone git project
    		Path tmpDir = Files.createTempDirectory("pkm-").toAbsolutePath();
    		String projectDir = GitCloner.cloneRepository(repo, tmpDir.toString(), true);

    		Properties config = ApiUtil.getPkmConfiguration();
    		config.setProperty("apikey", key);

    		String baseUrl = config.getProperty("baseUrl");
    		if(baseUrl == null) baseUrl = "https://pkm-api_pkm_1:8080";

    		// Initialize PKM client
    		PkmClient client;
    		client = new PkmClient(baseUrl, database, config.getProperty("certificate"));
    		client.login(key, true); // Do NOT log out!!!

    		// Load cloned project in PKM
    		PkmProjectLoader.loadProject(client, projectDir, "/",
    				extension, true,
    				System.out, System.err);

    		// Delete temp directory recursively
			// See https://www.baeldung.com/java-delete-directory
			Files.walk(tmpDir)
		      .sorted(Comparator.reverseOrder())
		      .map(Path::toFile)
		      .forEach(File::delete);

		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(405).body("{\"jmlgen\":\"" + ApiUtil.exceptionToSingleLine(e) + "\"}");
		}
    	
        return ResponseEntity.ok().body("{\"jmlgen\":\"Successful git clone of " + repo + " into " + database + "\"}");
    }

}
