package org.openapitools.api;

import org.openapitools.client.ApiException;
import org.springframework.web.context.request.NativeWebRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApiUtil {

    public static void setExampleResponse(NativeWebRequest req, String contentType, String example) {
        try {
            req.getNativeResponse(HttpServletResponse.class).addHeader("Content-Type", contentType);
            req.getNativeResponse(HttpServletResponse.class).getOutputStream().print(example);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private static Properties config = null;

    public static Properties getPkmConfiguration() {
    	if(ApiUtil.config == null) {
    		ApiUtil.config = new Properties();
    		try {
				ApiUtil.config.load(new FileInputStream("src/main/resources/jmlgen-pkm.properties"));
    			//ApiUtil.config.load(
    				//Thread.currentThread().getContextClassLoader().getResourceAsStream("jmlgen-pkm.properties"));
			} catch (Exception e) {
				e.printStackTrace(System.err);
			}
    	}
    	return ApiUtil.config;
    }

    public static void checkApiKey(NativeWebRequest req) {
       /* if (!"1".equals(System.getenv("DISABLE_API_KEY")) && !"special-key".equals(req.getHeader("api_key"))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Missing API key!");
        }*/
    }
    
    /**
     * Convert exception to single line log (eg. for JSON)
     * @param e The exception to convert
     * @return A single line containing the whole exception trace
     */
    public static String exceptionToSingleLine(Exception e) {
    	if(e == null) return "Unknown exception (null)";
    	String message = (e.getMessage() == null ? e.toString() : e.getMessage());
    	if(e instanceof ApiException) message = ((ApiException)e).getResponseBody() + " " + message;
    	StringBuilder buf = new StringBuilder(message);
    	StackTraceElement stack[] = e.getStackTrace();
    	if(e != null) {
    		for(int i=0; i< stack.length; i++) {
    			if(stack[i] != null) buf.append("\\n" + stack[i].toString());
    		}
    	}
    	return buf.toString();
    }
}