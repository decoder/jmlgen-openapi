package org.openapitools.api;

import org.openapitools.client.ApiException;
import org.openapitools.client.api.ProjectApi;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.decoder.jmlgen.PkmJmlGen;
import eu.decoder.pkm.client.PkmClient;
import java.util.Properties;

import javax.annotation.PostConstruct;


@Service
public class StatusApiDelegateImpl implements StatusApiDelegate {

    @PostConstruct
    private void init() {
    	
    }

    @Override
    public ResponseEntity<String> statusGet(String database, String key) {
    	if(key == null || key.trim().length() < 1) {
    		return ResponseEntity.status(401).body("{\"jmlgen\":\"api key null or empty\"}");
    	}
    	Properties config = ApiUtil.getPkmConfiguration();
    	config.setProperty("apikey", key);
    	PkmJmlGen jmlGenerator = new PkmJmlGen(config);
    	try {
			PkmClient client = jmlGenerator.login(database, false); // Do NOT log out!!!
			try {
				ProjectApi api = new ProjectApi(client.getApiClient());
				if(database != null) api.getProject(database, key);
			} catch(ApiException e) {
				if(! e.getMessage().toLowerCase().contains("not found")) throw e;
			}
	        return ResponseEntity.ok("{\"jmlgen\":\"ok\"}");
		} catch (ApiException e) {
			e.printStackTrace();
			return ResponseEntity.status(401).body("{\"jmlgen\":\"" + e.getMessage() + "\"}");
		}
    }

}
