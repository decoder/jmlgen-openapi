package org.openapitools.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.decoder.jmlgen.PkmJmlGen;
import eu.decoder.pkm.client.PkmClient;
import java.util.Properties;

import javax.annotation.PostConstruct;


@Service
public class LoginApiDelegateImpl implements LoginApiDelegate {

    @PostConstruct
    private void init() {
    	
    }

    @Override
    public ResponseEntity<String> loginPost(String user, String password) {
    	
    	Properties config = ApiUtil.getPkmConfiguration();
    	config.remove("apikey");
    	config.setProperty("user", user);
    	config.setProperty("password", password);
    	
    	String apiKey = null;
    	try {
    		PkmJmlGen jmlGenerator = new PkmJmlGen(config);
    		
    		// Login (and STAY LOGGED IN!) to generate an API key
    		PkmClient client = jmlGenerator.login("test", false);
    		apiKey = client.getApiKey();

    	} catch(Exception e ) {
    		e.printStackTrace(System.err);
			return ResponseEntity.status(401).body("{\"jmlgen\":\"" + ApiUtil.exceptionToSingleLine(e) + "\"}");
    	}
    	
    	if(apiKey == null) {
    		System.err.println("Error: API key is null!");
    		return ResponseEntity.status(500).body("{\"jmlgen\":\"api key null or empty\"}");
    	}

    	HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("key", apiKey);
    	return ResponseEntity.ok().headers(responseHeaders).body("{\"key\":\"" + apiKey + "\"}");
    }

    /*
    @Override
    public ResponseEntity<Void> logoutUser() {
        return ResponseEntity.ok().build();
    }*/

}
