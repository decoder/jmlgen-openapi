![](jmlgen-logo.png)

# JmlGen OpenApi service

API interface: https://gitlab.ow2.org/decoder/jmlgen-openapi/-/blob/master/src/main/resources/openapi.yaml

## Configuration / build

### Configuration

PKM connection configuration is available in src/main/resources/jmlgen-pkm.properties .
The file includes:
- PKM base URL
- PKM Credentials (user/password, path to the SSL certificate if any)

OpenApi service configuration is available in src/main/resources/application.properties .

Default values are set to connect to the docker-based PKM available at https://gitlab.ow2.org/decoder/pkm-api .
Default port for the OpenApi service is set to 5005 (to enable https, see src/main/resources/application.properties).


### Build

Build jmlgen-openapi using maven (after updating configuration if needed):

```
mvn clean install
```

## Run openapi service

```
mvn spring-boot:run
```

Then, connect to http://localhost:5005 (unless configuration has been changed).

To obtain an API key (necessary to pass in "key" HTTP header), jmlgen-openapi proposes a "/jmlgen/login" endpoint that logs in then returns an API key (and puts it in the response headers).
This is normally for test purpose: login should be done by external callers, and API key passed in the HTTP headers.

Example to obtain an API key using curl:
```
curl -X POST "http://localhost:5005/jmlgen/login?user=admin&password=admin" -H  "accept: application/json"
```
A successful response body looks like this (the key being the value of "key" attribute):

```
{
  "key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInVzZXJfcGFzc3dvcmQiOiJhZG1pbiIsImlhdCI6MTYxMTMwOTAyMX0.G2wORi69nlkOQBCPXIpjsfJcjVR0INBrC_4RDsUFpwc"
}
```

JmlGen endpoints (both require a "key" HTTP header with a valid API key):
- /jmlgen/status to perform checks
- /jmlgen/generate to generate JML (parameter: database name - mandatory)

## Notes for MyThaiStar sample

To run a sample, it is recommended to run the PKM using https://gitlab.ow2.org/decoder/pkm-api (see documentation: after build, launch with "docker compose up").

The pkm-simple-java-client project can be used to upload the MyThaiStar sample in the PKM: see https://gitlab.ow2.org/decoder/pkm-simple-java-client#upload-a-project-in-the-pkm .

When done, specify OW2-MyThaiStar as a database name to OpenApi /jmlgen/generate endpoint.

To check that JML was properly generated and put in the PKM, one can query RawSourceCode collection in DB OW2-MyThaiStar as follows (MongoDB query):

```
db.getCollection('RawSourcecode').find({'filename':'java/mtsj/api/src/main/java/com/devonfw/application/mtsj/general/logic/api/to/BinaryObjectEto.java'})
```

## Integration tests

Integration tests are provided in test/ directory.

The global test (jmlgen_test.sh) proposes to clone my-thai-star sample project out of Gitlab and store it in the PKM.
Then, it generates JML inside, checks that some JML was inserted, and proposes to delete the project.
By default, it uses the online version of the JmlGen service and PKM hosted at OW2, but can also work with a local installation.

Simply run it, and follow instructions:

```
jmlgen_test.sh
```

Another script (jmlgen_create_project.sh) can be used independently, to clone a Git project into the PKM: it receives 2 arguments, the git clone URL and the PKM destination project name (default values clone my-thai-star sample project into jmlgen-mythaistar).

For example, to clone OW2 sat4j project into the PKM:

```
jmlgen_create_project.sh https://gitlab.ow2.org/sat4j/sat4j.git ow2-sat4j
```

## Build/run docker image

```
docker build -t "jmlgen" -t "gitlab.ow2.org:4567/decoder/jmlgen-openapi:master" .
docker run --name jmlgen -p 5005:5005 -dit jmlgen
```

To push image on OW2 gitlab (after build):

```
docker login gitlab.ow2.org:4567
docker push gitlab.ow2.org:4567/decoder/jmlgen-openapi:master
```

## Generate keystore for Spring security (if SSL enabled)

Starting from pkm-api/ directory:

```
openssl pkcs12 -export -in ssl/pkm_docker.crt -inkey ssl/pkm_docker.key -name tomcat -out keystore.p12
```
(use "password" as password).

Then copy keystore.p12 in src/main/resources .

## Note concerning SNAPSHOT dependencies

Dependencies related to Jml generation and PKM integration are managed on the Gitlab package registry.
The dependency management mechanism should be transparent (like for atitacts deployed on maven central).

If not, the alternative is: clone each project, and build it (mvn clean install).
Dependencies required:
- jmlgen: https://gitlab.ow2.org/decoder/jmlgen
- PKM simple java client: https://gitlab.ow2.org/decoder/pkm-simple-java-client
- jmlgen-pkm: https://gitlab.ow2.org/decoder/jmlgen-pkm (this dependency is present in the pom, but points on the library quoted above)

