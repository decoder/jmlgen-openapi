#!/usr/bin/env bash

if [ x$PKM_HOST == x ]; then
  echo -n "PKM_HOST[:port]: (default https://decoder-tool.ow2.org/pkm): "
  read PKM_HOST
  if [[ $PKM_HOST == https://localhost* ]]; then
    PKM_HOST=$(echo $PKM_HOST | sed -e "s/localhost/pkm-api_pkm_1/")
  fi
  if [ x$PKM_HOST == x ]; then
    PKM_HOST="https://decoder-tool.ow2.org/pkm"
  fi
  echo -n "JMLGEN_SERVICE_URI: (default https://decoder-tool.ow2.org/jmlgen): "
  read JMLGEN_SERVICE_URI
  if [ x$JMLGEN_SERVICE_URI == x ]; then
    JMLGEN_SERVICE_URI="https://decoder-tool.ow2.org/jmlgen"
  fi
  echo "Using PKM at ${PKM_HOST} and JmlGen service at ${JMLGEN_SERVICE_URI} (/jmlgen)"
  JMLGEN_SERVICE_URI="${JMLGEN_SERVICE_URI}/jmlgen"
fi

set -o errexit
set -o pipefail
set -o nounset

API=$(cd $(dirname $0)/..; pwd)
COMPRESSED=yes
TRACE=yes
PRINT_CURL_CMD=no
CURL_MAX_TIME=600 # seconds

CERT="pkm_docker.crt"

tracefile=$0
tracefile="/tmp/${tracefile%.*}.trace"

function exit_status
{
    local STATUS=$1
    if [ $STATUS -eq 0 ]; then
        echo "Test PASSED" >&2
    else
        echo "Test FAILED" >&2
    fi

    exit $STATUS
}

function return_status
{
	if [ $1 -ne 0 ]; then
		exit_status $1
	fi
}

return_status=return_status

function mr_proper_prompt
{
  set +o nounset
  if [ "${MR_PROPER,,}" != 'yes' ] && [ "${MR_PROPER,,}" != 'y' ] && [ "${MR_PROPER,,}" != 'no' ] && [ "${MR_PROPER,,}" != 'n' ]; then
    echo -n "Do you want to clean before exiting (yes/no) ? "
    read MR_PROPER
  fi
  set -o nounset
  
  if [ "${MR_PROPER,,}" = 'yes' ] || [ "${MR_PROPER,,}" = 'y' ]; then
    return 0
  else
    return 1
  fi
}

function uri_encode
{
  echo -n $1 | jq -sRr @uri
}

function pretty_print_json
{
  if [ $# -ne 0 ]; then
    jq . "$1"
  else
    jq .
  fi
}

# remove end of line and extra spaces from a JSON file
# example:              
# {                     |
#   "a" : "string",     |==>    {"a":"string","b":1}
#   "b" : 1             |
# }                     
function compact_json
{
  if [ $# -ne 0 ]; then
    jq -c . "$1"
  else
    jq .
  fi
}

# convert a text file to a JSON string
# example:              
# hello     |
# world!    |==>    "hello\nworld!\n"
#           |     
function to_json_string
{
  if [ $# -ne 0 ]; then
    jq -Rs . "$1"
  else
    jq -Rs .
  fi
}

# build_file_list(&FILE_LIST, STARTING_POINT, ...PATTERNS)
function build_file_list
{
  local -n __FILE_LIST_REF__=$1
  shift
  local STARTING_POINT=$1
  shift
  local FIND_EXPR=()
  for PATTERN in "$@"; do
    if [[ "${PATTERN}" == *'/'* ]]; then
      if [ -f "${STARTING_POINT}/${PATTERN}" ]; then
        __FILE_LIST_REF__+=( $(realpath "${STARTING_POINT}/${PATTERN}") )
      else
        __FILE_LIST_REF__+=( $(realpath "${PATTERN}") )
      fi
    else
      if [ ${#FIND_EXPR[@]} -ne 0 ]; then
        FIND_EXPR+=( '-o' )
      fi
      FIND_EXPR+=( '-name' "${PATTERN}" '-print0' )
    fi
  done
  if [ ${#FIND_EXPR[@]} -ne 0 ]; then
    local FIND_CMD=( 'find' "${STARTING_POINT}" "${FIND_EXPR[@]}" )
    local FILE
    while IFS= read -d $'\0' -r FILE ; do
      __FILE_LIST_REF__+=( "${FILE}" )
    done < <("${FIND_CMD[@]}")
  fi
}

function generate_random_password
{
	# set of all printable ASCII characters but space, single quote, double quote, backquote, and backslash
	chars='!#$%&()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz{|}~'
	# generate a password of 8 characters
	for i in {1..8} ; do
			echo -n "${chars:RANDOM%${#chars}:1}"
	done
}

function call_curl
{
		if [ "${TRACE}" = "yes" ] && [ ! -f "${tracefile}" ]; then
				echo -n "" > ${tracefile}
		fi
    local CURL=$(which curl)
    local TRACE_FILE
    local OUT_FILE
    local HEADERS_FILE
    local args=("$@")
    local i=0
    local CURL_CMD=("$CURL")
    if [ "${COMPRESSED}" = "yes" ]; then
        CURL_CMD+=('--compressed')
    fi
    if [ "${TRACE}" = "yes" ]; then
        TRACE_FILE=$(mktemp -t curl.XXXXX)
        CURL_CMD+=('--trace-ascii' "${TRACE_FILE}")
    fi
    CURL_CMD+=('-s' '-S')
    CURL_CMD+=('-H'  'accept: application/json')
    CURL_CMD+=('-H'  'Content-Type: application/json')
    OUT_FILE=$(mktemp -t curl.XXXXX)
    CURL_CMD+=('-o' "${OUT_FILE}")
    HEADERS_FILE=$(mktemp -t curl.XXXXX)
    CURL_CMD+=('-D' "${HEADERS_FILE}")
    CURL_CMD+=('--cacert' "${API}/src/main/resources/${CERT}")
    CURL_CMD+=('-m' "${CURL_MAX_TIME}")

    while [ $i -lt $# ]; do
        CURL_CMD+=("${args[$i]}")
        i=$(($i+1))
    done
    if [ "${PRINT_CURL_CMD}" = 'yes' ]; then
      echo "${CURL_CMD[@]}" >&2
    fi
    "${CURL_CMD[@]}"
    if [ $? -ne 0 ]; then
        echo >&2
        echo "call to pkm failed" >&2
        $return_status 1
    fi
    http_code=$(grep "HTTP/1.1" ${HEADERS_FILE} | tail -n 1 | sed -e "s/HTTP\/1\.1 //" -e "s/ .*//")
    local STATUS=0
    if [ ${http_code} -lt 300 ] ;
    then
        STATUS=0 ;
        pretty_print_json ${OUT_FILE} || cat ${OUT_FILE}
    else
        echo "Call to PKM server failed:" >&2
        printf "'%s' " "${CURL_CMD[@]}" >&2
        echo >&2
        cat ${HEADERS_FILE} >&2
        cat ${OUT_FILE} >&2
        STATUS=${http_code} ;
    fi
    if [ "${TRACE}" = "yes" ]; then
        printf "'%s' " "${CURL_CMD[@]}" >> ${tracefile}
        cat "${TRACE_FILE}" >> ${tracefile}
        rm -f "${TRACE_FILE}"
        cat "${HEADERS_FILE}" >> ${tracefile}
        cat "${OUT_FILE}" >> ${tracefile}
    fi

    rm -f "${HEADERS_FILE}"
    rm -f "${OUT_FILE}"
#     echo "curl returns ${STATUS}" >&2
    $return_status "${STATUS}"
}

function call_curl_get
{
  if [ "$#" -ne 2 ]; then
      echo "Error in call_curl_get function: takes 2 arguments: acces key and pkm path" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PKM_PATH=$2
  if ! call_curl -X GET "${PKM_HOST}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}"; then
      echo "calling get on pkm failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function call_curl_post
{
  if [ "$#" -ne 3 ]; then
      echo "Error in call_curl_post function: takes 3 arguments: access key, pkm path and content to post" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PKM_PATH=$2
  local DATA=$3
  if ! call_curl -X POST "${PKM_HOST}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}" --data-raw "${DATA}"; then
      echo "calling post on pkm path ${PKM_PATH} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function call_curl_put
{
  if [ "$#" -ne 3 ]; then
      echo "Error in call_curl_put function: takes 3 arguments: acces key, pkm path and content to put" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PKM_PATH=$2
  local DATA=$3
  if ! call_curl -X PUT "${PKM_HOST}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}" --data-raw "${DATA}"; then
      echo "calling put on pkm path ${PKM_PATH} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function login
{
  if [ "$#" -ne 2 ]; then
      echo "Error in login function: takes 2 arguments: user name and password" >&2
      $return_status 1
  fi
  local USER_NAME=$1
  local PASSWORD=$2
  echo "user's login" >&2
  local USER_KEY
  USER_KEY=$(call_curl -X POST "${PKM_HOST}/user/login" -d "{\"user_name\":\"${USER_NAME}\",\"user_password\":\"${PASSWORD}\"}" | jq .key | sed -n 's/^"\([^"]*\)"$/\1/p')
  if [ $? -ne 0 ]; then
      echo >&2
      echo "user's login failed" >&2
      $return_status 1
  fi
  echo "${USER_KEY}"
  $return_status 0
}

function jmlgen_login
{
  if [ "$#" -ne 2 ]; then
      echo "Error in jmlgen_login function: takes 2 arguments: user name and password" >&2
      $return_status 1
  fi
  local USER_NAME=$1
  local PASSWORD=$2
  local USER_KEY
  USER_KEY=$(call_curl -X POST "${JMLGEN_SERVICE_URI}/login?user=${USER_NAME}&password=${PASSWORD}" | jq .key | sed -n 's/^"\([^"]*\)"$/\1/p')
  if [ $? -ne 0 ]; then
      echo >&2
      echo "user's login failed" >&2
      $return_status 1
  fi
  echo "${USER_KEY}"
  $return_status 0
}

function jmlgen_gitclone
{
  if [ "$#" -ne 3 ]; then
      echo "Error in jmlgen_gitclone function: takes 3 arguments: access key, project name, repo URL" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local REPO=$3
  call_curl -X POST "${JMLGEN_SERVICE_URI}/gitclone?db=${PROJECT_NAME}&repo=$(uri_encode ${REPO})&extension=.java" -H  "key: $ADMIN_KEY"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Cloning ${REPO} into ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function jmlgen_generate
{
  if [ "$#" -ne 2 ]; then
      echo "Error in jmlgen_generate function: takes 2 arguments: access key, project name" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  call_curl -X POST "${JMLGEN_SERVICE_URI}/generate?db=${PROJECT_NAME}" -H  "key: $ADMIN_KEY"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Generating JML into ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function project_post
{
  if [ "$#" -ne 4 ]; then
      echo "Error in project_post function: takes 4 arguments: access key, project name, owner name and owner roles" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local OWNER=$3
  local ROLES=$4
  echo "creating project ${PROJECT_NAME} for user ${OWNER}" >&2
  call_curl -X POST "${PKM_HOST}/project" -H  "key: $ADMIN_KEY" -d "{\"name\":\"${PROJECT_NAME}\",\"members\":[{\"name\":\"${OWNER}\",\"owner\":true,\"roles\":[${ROLES}]}]}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Creating project${PROJECT_NAME} for user ${OWNER} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function project_get
{
  if [ "$#" -ne 2 ]; then
      echo "Error in project function: takes 2 arguments: access key and project name" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  echo "getting project ${PROJECT_NAME}" >&2
  local PROJECT
  PROJECT=$(call_curl -X GET "${PKM_HOST}/project/${PROJECT_NAME}" -H  "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting project ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  echo "${PROJECT}"
  $return_status 0
}

function comments_get
{
  if [ "$#" -ne 4 ]; then
      echo "Error in comments_get function: takes 4 arguments: access key, project name, language name and file name" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local LANGUAGE=$3
  local FILE_NAME=$4
  echo "getting comments for ${FILE_NAME} in project ${PROJECT_NAME}" >&2
  local COMMENTS
  COMMENTS=$(call_curl -X GET "${PKM_HOST}/code/${LANGUAGE}/comments/${PROJECT_NAME}/$(uri_encode ${FILE_NAME})" -H  "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting comments ${FILE_NAME} in ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  echo "${COMMENTS}"
  $return_status 0
}


function annotations_get
{
  if [ "$#" -ne 3 ]; then
      echo "Error in annotations_get function: takes 3 arguments: access key, project name and path" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local PKM_PATH=$3
  echo "getting annotations for ${PKM_PATH} in project ${PROJECT_NAME}" >&2
  local ANNOTATIONS
  ANNOTATIONS=$(call_curl -X GET "${PKM_HOST}/annotations/${PROJECT_NAME}/$(uri_encode ${PKM_PATH})" -H "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting annotations ${PKM_PATH} in ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  echo "${ANNOTATIONS}"
  $return_status 0
}

function annotations_post
{
  if [ "$#" -ne 4 ]; then
      echo "Error in annotations_post function: takes 4 arguments: access key, project name, path and annotations" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local PKM_PATH=$3
  local ANNOTATIONS=$4
  echo "posting annotations ${ANNOTATIONS} for ${PKM_PATH} in project ${PROJECT_NAME}" >&2
  call_curl -X POST "${PKM_HOST}/annotations/${PROJECT_NAME}/$(uri_encode ${PKM_PATH})" -H  "key: ${ACCESS_KEY}" -d "[${ANNOTATIONS}]"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "posting annotations ${ANNOTATIONS} to ${PKM_PATH} in ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

function rawsourcecode_post
{
  if [ "$#" -ne 4 ]; then
      echo "Error in rawsourcecode_post function: takes 4 arguments: access key, project name, root directory and file path" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local ROOT_DIR=$3
  local FILE_PATH=$4
  echo "posting raw source code ${FILE_PATH} to project ${PROJECT_NAME}" >&2
  post_files "${ACCESS_KEY}" "code/rawsourcecode/${PROJECT_NAME}" 'text' "${ROOT_DIR}" "${FILE_PATH}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "posting raw source code ${FILE_PATH} to project ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

function user_post
{
  if [ "$#" -ne 3 ]; then
      echo "Error in user_post function: takes 3 arguments: access key, username and password" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local USER_NAME=$2
  local PASSWORD=$3
  echo "creating user ${USER_NAME} with password XXXXX" >&2
  call_curl -X POST "${PKM_HOST}/user" -H  "key: $ACCESS_KEY" -d "{\"password\":\"${PASSWORD}\",\"phone\":\"phone\",\"name\":\"${USER_NAME}\",\"last_name\":\"last_name\",\"first_name\":\"first_name\",\"email\":\"email\"}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Creating user ${USER_NAME} with password XXXXX failed." >&2
      $return_status 1
  fi
  $return_status 0
}

function delete
{
  if [ "$#" -ne 2 ]; then
      echo "Error in delete function: takes 2 arguments: access key and path to delete" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PATH_TO_DEL=$2
  echo "deleting ${PATH_TO_DEL}" >&2
  call_curl -X DELETE "${PKM_HOST}/${PATH_TO_DEL}" -H  "key: $ACCESS_KEY"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "deleting ${PATH_TO_DEL} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

# post_files(ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)

# send_files(METHOD, ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)
function send_files
{
  if [ "$#" -lt 6 ]; then
      echo "Error in send_files function: takes at least 6 arguments: method, access key, a pkm path, a format ('text' or 'binary'), a root directory, and one or more file patterns" >&2
      $return_status 1
  fi

  local METHOD="$1"
  shift
  local ACCESS_KEY="$1"
  shift
  local PKM_PATH="$1"
  shift
  local FORMAT="$1"
  shift
  local ROOT_DIR
  ROOT_DIR=$(realpath "$1")
  if [ $? -ne 0 ]; then
    $return_status 1
  fi
  shift
  
  if [ "${FORMAT}" != 'text' ] && [ "${FORMAT}" != 'binary' ]; then
    echo "Error in post_files function: format shall be 'text' or 'binary', got '${FORMAT}'" >&2
    $return_status 1
  fi
  
  local FILES=()
  build_file_list FILES "${ROOT_DIR}" "$@"
  
  local FILE_OBJECTS=()
  local FIRST_FILE_IN_BUNDLE='yes'
  local FILE_OBJECT_SEPARATOR=
  for FILE in "${FILES[@]}"; do
    local REL_PATH
    REL_PATH=$(realpath --relative-to="${ROOT_DIR}" "${FILE}")
    if [ $? -ne 0 ]; then
      $return_status 1
    fi
    echo "sending (${METHOD}) ${REL_PATH}" >&2
    if [ "${FIRST_FILE_IN_BUNDLE}" = 'yes' ]; then
      FILE_OBJECT_SEPARATOR=
      FIRST_FILE_IN_BUNDLE='no'
    else
      FILE_OBJECT_SEPARATOR=','
    fi
    local FILE_OBJECT=$(mktemp -t curl.XXXXX)
    FILE_OBJECTS+=("${FILE_OBJECT}")
    (
      echo -n "${FILE_OBJECT_SEPARATOR}"
      if [ "${FORMAT}" = 'binary' ]; then
        echo -n "{\"format\":\"binary\""
      else
        echo -n "{\"format\":\"text\",\"encoding\":\"utf8\""
      fi
      echo -n ",\"rel_path\":\"${REL_PATH}\",\"content\":"
      if [ "${FORMAT}" = 'binary' ]; then
        base64 -w 0 "${FILE}"
      else
        to_json_string "${FILE}"
      fi
      echo -n "}"
    ) >> "${FILE_OBJECT}"&
  done
  
  wait
  
  local BODY=$(mktemp -t curl.XXXXX)
  ( echo -n "["
    cat "${FILE_OBJECTS[@]}"
    echo -n "]"
  ) | gzip -5 > "${BODY}"
  rm -f "${FILE_OBJECTS[@]}"
  
  if ! call_curl -X "${METHOD}" "${PKM_HOST}/${PKM_PATH}" -H  "key: $ACCESS_KEY" -H "Content-Encoding: gzip" --data-binary @"${BODY}"; then
    rm -f "${BODY}"
    echo "calling ${METHOD} on pkm path ${PKM_PATH} failed" >&2
    $return_status 1
  fi
  
  rm -f "${BODY}"
  
  $return_status 0
}

# post_files(ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)
function post_files
{
  if [ "$#" -lt 5 ]; then
      echo "Error in post_files function: takes at least 5 arguments: access key, a pkm path, a format ('text' or 'binary'), a root directory, and one or more file patterns" >&2
      $return_status 1
  fi
  
  send_files 'POST' "$@"
}

# put_files(ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)
function put_files
{
  if [ "$#" -lt 5 ]; then
      echo "Error in put_files function: takes at least 5 arguments: access key, a pkm path, a format ('text' or 'binary'), a root directory, and one or more file patterns" >&2
      $return_status 1
  fi
  
  send_files 'PUT' "$@"
}

# compile_commands_post_file(ACCESS_KEY, PROJECT_NAME, FILE)
function compile_commands_post_file
{
  if [ "$#" -ne 3 ]; then
      echo "Error in compile_commands_post_file function: takes 3 arguments: access key, project name, and a compile_commands.json filename" >&2
      $return_status 1
  fi
  local ACCESS_KEY="$1"
  local PROJECT_NAME="$2"
  local FILE="$3"
  echo "posting compile commands from ${FILE} in project ${PROJECT_NAME}" >&2
  call_curl -X POST "${PKM_HOST}/compile_command/${PROJECT_NAME}" -H  "key: ${ACCESS_KEY}" --data-binary @"${FILE}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "posting compile commands from ${FILE} in project ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

