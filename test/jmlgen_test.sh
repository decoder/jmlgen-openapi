#!/usr/bin/env bash

##############################################################
## Global JmlGen test                                       ##
## - Clone my-thai-star project into PKM (if requested)     ##
## - Generate JML (requires my-thai-star previously cloned) ##
##############################################################

DIR=$(cd $(dirname $BASH_SOURCE); pwd)

if [ "${CREATE_PROJECT,,}" != 'yes' ] && [ "${CREATE_PROJECT,,}" != 'y' ] && [ "${CREATE_PROJECT,,}" != 'no' ] && [ "${CREATE_PROJECT,,}" != 'n' ]; then
  echo -n "Do you want to (re)create/clone the jmlgen-mythaistar project (yes/no) ? "
  read CREATE_PROJECT
fi

if [ "${CREATE_PROJECT,,}" = 'yes' ] || [ "${CREATE_PROJECT,,}" = 'y' ]; then
  source "${DIR}/jmlgen_create_project.sh"
fi

source "${DIR}/jmlgen_generate_jml.sh"

if mr_proper_prompt; then
  echo "cleaning"
  
  echo "Deleting project jmlgen-mythaistar"
  delete "${ADMIN_KEY}" "project/jmlgen-mythaistar"
  echo
fi

exit ${EXIT_STATUS}

