#!/usr/bin/env bash

GIT_URL=$1
if [ x${GIT_URL} == x ]; then
 GIT_URL="https://github.com/devonfw/my-thai-star"
fi

DB_NAME=$2
if [ x${DB_NAME} == x ]; then
 DB_NAME="jmlgen-mythaistar"
fi
echo "Requested to clone project ${GIT_URL} into ${DB_NAME}"

if [ x${ADMIN_KEY} == x ]; then
  DIR=$(cd $(dirname $BASH_SOURCE); pwd)
  source "${DIR}/pkm_rest_api.sh"

  ADMIN_NAME="admin"
  echo -n "${ADMIN_NAME}'s password: "
  read -s ADMIN_PASSWORD

  echo "PKM administrator's login"
  ADMIN_KEY=$(jmlgen_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
  [ $? -eq 0 ] || exit $?
fi

set +o errexit

function exit_status
{
    local STATUS=$1
    if [ $STATUS -eq 0 ]; then
        echo "Test PASSED" >&2
    else
        echo "Test FAILED" >&2
    fi

    EXIT_STATUS=$STATUS
    
    return $STATUS
}

return_status=return

while true; do

  echo "(re)creating ${DB_NAME} project"
  delete "${ADMIN_KEY}" "project/${DB_NAME}"
  call_curl_post "${ADMIN_KEY}" "project" "{\"name\":\"${DB_NAME}\"}" || exit_status 1 || break

  echo "Cloning ${GIT_URL} into ${DB_NAME} project"
  jmlgen_gitclone "${ADMIN_KEY}" "${DB_NAME}" "${GIT_URL}" || exit_status 1 || break
  echo

  exit_status 0
  
  break
done

