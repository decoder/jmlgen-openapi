#!/usr/bin/env bash

if [ x${ADMIN_KEY} == x ]; then
  DIR=$(cd $(dirname $BASH_SOURCE); pwd)
  source "${DIR}/pkm_rest_api.sh"

  ADMIN_NAME="admin"
  echo -n "${ADMIN_NAME}'s password: "
  read -s ADMIN_PASSWORD

  echo "PKM administrator's login"
  ADMIN_KEY=$(jmlgen_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
  [ $? -eq 0 ] || exit $?
fi

set +o errexit

function exit_status
{
    local STATUS=$1
    if [ $STATUS -eq 0 ]; then
        echo "Test PASSED" >&2
    else
        echo "Test FAILED" >&2
    fi

    EXIT_STATUS=$STATUS
    
    return $STATUS
}

return_status=return

while true; do

  echo "Generating JML into project jmlgen-mythaistar"
  jmlgen_generate "${ADMIN_KEY}" jmlgen-mythaistar || exit_status 1 || break
  echo

  echo "checking JML generation"
  call_curl_get "${ADMIN_KEY}" "code/rawsourcecode/jmlgen-mythaistar/java%2Fsalesforcepoc%2Fsrc%2Fmain%2Fjava%2Fcom%2Fcapgemini%2Fsalesforcepoc%2Fdto%2FLead.java" | grep "\/\*\@ pure \@\*\/" || exit_status 1 || break
  echo

  exit_status 0
  
  break
done

