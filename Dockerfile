# Lightweight container based on Alpine distro
FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine

# Create user so not to run process as root
RUN addgroup -g 2010 apiuser && \
adduser --uid 2011 --ingroup apiuser --disabled-password apiuser

EXPOSE 5005

WORKDIR /jmlgen
COPY target/jmlgen-openapi-0.0.1-SNAPSHOT.jar /jmlgen/jmlgen-openapi.jar

# Configure webapp (first default config, then overwrite with specific one)
COPY src/main/resources /jmlgen
COPY docker-resources/config/* /jmlgen/src/main/resources/

COPY docker-resources/script/start.sh /jmlgen
RUN chmod +x /jmlgen/start.sh
RUN chown -R apiuser:apiuser /jmlgen

# Switch to new user
USER apiuser

CMD ["java", "-jar", "/jmlgen/jmlgen-openapi.jar"]

